class Game {
  constructor(inputWord) {
    this.timer = 8
    this.inputWord = inputWord
    this.view = new ViewController(inputWord, this.timer)
    this.guessedLetters = []
    this.letterSet = Array.from(new Set(inputWord))
    this.hangmanCanvas = new HangmanCanvas()
  }

  validateInputWord() {
    if (!this.inputWord.match(/^[a-z]*$/)) {
      alert("only alphabets allowed")
      return false
    }
    if (this.inputWord.length > 0 && this.inputWord.length < 21) {
      return true
    } else {
      alert("enter a word with 1~20 letters")
    }
  }

  validateInputLetter(inputLetter) {
    if (inputLetter.match(/^[a-z]*$/) && inputLetter.length === 1) {
      return true
    } else {
      alert("enter an alphabet")
    }
  }

  judgeInputLetter(inputLetter) {
    this.guessedLetters.push(inputLetter)
    if (this.inputWord.match(inputLetter)) {
      this.letterSet.pop(inputLetter)
      return true
    }
  }

  countDown() {
    this.timer--
    this.view.updateTimer(this.timer)
  }

  checkGameStatus() {
    if (this.timer === 0) {
      return "gameover"
    } else if (this.letterSet.length == 0) {
      return "clear"
    }
  }

  run() {
    if (this.validateInputWord()) {
      this.view.showGuessPage()
    }

    document.getElementById("guess_letter_form").addEventListener(
      "submit",
      event => {
        const inputLetter = document
          .getElementById("guess_letter")
          .value.toLowerCase()
        document.guess_letter_form.reset()
        if (this.validateInputLetter(inputLetter)) {
          if (this.guessedLetters.indexOf(inputLetter) >= 0) {
            alert("already used the letter")
            event.preventDefault()
            return false
          }

          if (this.judgeInputLetter(inputLetter)) {
            this.view.addLetterToLines(inputLetter)
          } else {
            this.view.addIncorrectLetter(inputLetter)
            this.hangmanCanvas.drawHangman()
            this.countDown()
          }

          if (this.checkGameStatus() === "clear") {
            this.view.showCongratulation()
            this.view.disableSubmit()
          } else if (this.checkGameStatus() === "gameover") {
            this.view.showGameOver()
            this.view.disableSubmit()
          }
        }
        event.preventDefault()
      },
      false
    )
    document
      .getElementById("next_game")
      .addEventListener("submit", () => location.reload(), false)
  }
}

class ViewController {
  constructor(inputWord, timer) {
    this.inputWord = inputWord
    this.inputWordLetterArray = inputWord.split("")
    this.wordStatus = []
    this.incorrectWords = []
    this.timer = timer
  }

  showLines() {
    for (let i = 0; i < this.inputWord.length; i++) {
      this.wordStatus.push("__ ")
    }
    return this.wordStatus.join("")
  }

  showGuessPage() {
    document.getElementById("input_phase").style.display = "none"
    document.getElementById("guess_phase").style.visibility = "visible"
    document.getElementById("number_of_letter").innerHTML = this.showLines()
  }

  addLetterToLines(inputLetter) {
    for (let i = 0; i < this.wordStatus.length; i++) {
      // 文字リストと入力された文字を比較し、一致していたらその文字に置き換える
      if (this.inputWordLetterArray[i] == inputLetter) {
        this.wordStatus[i] = inputLetter
      }
      document.getElementById(
        "number_of_letter"
      ).innerHTML = this.wordStatus.join("")
    }
  }

  addIncorrectLetter(inputLetter) {
    this.incorrectWords.push(inputLetter)
    document.getElementById("wrong_letters").innerHTML = this.incorrectWords
  }

  updateTimer(timer) {
    this.timer = timer
  }

  disableSubmit() {
    document.getElementById("guess_submit_button").disabled = true
  }

  showGameOver() {
    document.getElementById("result").style.visibility = "visible"
    document.getElementById(
      "correct_word"
    ).innerHTML = `Nice try! The word was... ${this.inputWord} !!`
  }

  showCongratulation() {
    document.getElementById("result").style.visibility = "visible"
    document.getElementById(
      "correct_word"
    ).innerHTML = `Congratulations! The word was... ${this.inputWord} !!`
  }
}

class HangmanCanvas {
  constructor() {
    this.canvas = document.getElementById("canvas")
    this.ctx = this.canvas.getContext("2d")
    this.border = this.drawBorder()
    this.drawCount = 0
    this.drawPart = [
      this.pole,
      this.rope,
      this.head,
      this.body,
      this.leftArm,
      this.rightArm,
      this.leftLeg,
      this.rightLeg
    ]
  }

  drawBorder() {
    this.ctx.strokeRect(10, 10, 310, 300)
    this.defaultStatus = this.drawDefault()
  }

  drawDefault() {
    this.ctx.beginPath()
    this.ctx.moveTo(30, 270)
    this.ctx.lineTo(130, 270)
    this.ctx.closePath()
    this.ctx.stroke()

    this.ctx.beginPath()
    this.ctx.moveTo(80, 50)
    this.ctx.lineTo(80, 270)
    this.ctx.closePath()
    this.ctx.stroke()

    this.ctx.beginPath()
    this.ctx.moveTo(45, 270)
    this.ctx.lineTo(80, 220)
    this.ctx.closePath()
    this.ctx.stroke()

    this.ctx.beginPath()
    this.ctx.moveTo(115, 270)
    this.ctx.lineTo(80, 220)
    this.ctx.closePath()
    this.ctx.stroke()
  }

  drawHangman() {
    this.drawPart[this.drawCount](this.ctx)
    this.drawCount++
  }

  pole(context) {
    context.beginPath()
    context.moveTo(80, 50)
    context.lineTo(220, 50)
    context.closePath()
    context.stroke()
  }

  rope(context) {
    context.beginPath()
    context.moveTo(220, 50)
    context.lineTo(220, 80)
    context.closePath()
    context.stroke()
  }

  head(context) {
    context.beginPath()
    context.arc(220, 100, 20, 0, 2 * Math.PI)
    context.closePath()
    context.stroke()
  }

  body(context) {
    context.beginPath()
    context.moveTo(220, 120)
    context.lineTo(220, 180)
    context.closePath()
    context.stroke()
  }

  leftArm(context) {
    context.beginPath()
    context.moveTo(220, 145)
    context.lineTo(185, 120)
    context.closePath()
    context.stroke()
  }

  rightArm(context) {
    context.beginPath()
    context.moveTo(220, 145)
    context.lineTo(255, 120)
    context.closePath()
    context.stroke()
  }

  leftLeg(context) {
    context.beginPath()
    context.moveTo(220, 180)
    context.lineTo(195, 220)
    context.closePath()
    context.stroke()
  }

  rightLeg(context) {
    context.beginPath()
    context.moveTo(220, 180)
    context.lineTo(245, 220)
    context.closePath()
    context.stroke()
  }
}

window.onload = () => {
  document.getElementById("input_form").addEventListener(
    "submit",
    event => {
      const input_word = document
        .getElementById("input_word")
        .value.toLowerCase()
      const game = new Game(input_word)
      game.run()
      event.preventDefault()
    },
    false
  )
}
